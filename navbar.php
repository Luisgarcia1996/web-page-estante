<header>
        <div id="header-one" class="header header-layout1 header-fixed">
            <!-- <div class="header-top-area-dark header-top-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-8 col-sm-7">
                            <ul class="top-bar-contact-layout1">
                                <li><i class="bi bi-phone" aria-hidden="true"></i><a href="Tel:++6789-875-2235"> Call:+6789-875-2235</a></li>
                                <li><i class="bi bi-envelop" aria-hidden="true"></i><a href="#"> Info.domain@info.com</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-6 col-md-4 col-sm-5">
                            <ul class="top-bar-social-layout2">
                                <li><a href="#" title="facebook"><i class="icofont icofont-social-facebook"></i></a></li>
                                <li><a href="#" title="twitter"><i class="icofont icofont-social-twitter"></i></a></li>
                                <li><a href="#" title="google"><i class="icofont icofont-social-google-plus"></i></a></li>
                                <li><a href="#" title="dribbble"><i class="icofont icofont-social-dribbble"></i></a></li>
                                <li><a href="#" title="linkedin"><i class="icofont icofont-social-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="main-menu-area bg-light" id="sticker">
                <div class="container">
                    <div class="row d-md-flex align-items-md-center">
                        <div class="col-lg-2 col-md-2">
                            <div class="logo-area">
                                <a href="index.html"><img src="img/logo.png" alt="logo" class="img-responsive"></a>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9">
                            <nav id="dropdown">
                                <ul class="text-uppercase text-right">
                                    <li><a class="active" href="#">INICIO</a>
                                        <ul>
                                            <li class="active"><a href="index.html">Home 1</a></li>
                                            <li><a href="index2.html">Home 2</a></li>
                                            <li><a href="index3.html">Home 3</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="about.html">NOSOTROS</a></li>
                                    <li><a href="#">SERVICIOS</a>
                                        <ul>
                                            <li><a href="services.html">Services</a></li>
                                            <li><a href="services-Details.html">Services Details</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">PROYECTOS</a>
                                        <ul>
                                            <li><a href="project1.html">Projects 1</a></li>
                                            <li><a href="project2.html">Projects 2</a></li>
                                            <li><a href="project3.html">Projects 3</a></li>
                                            <li><a href="project4.html">Projects 4</a></li>
                                            <li><a href="project-details1.html">Project Details 1</a></li>
                                            <li><a href="project-details2.html">Project Details 2</a></li>
                                        </ul>
                                    </li>
                                    <!-- <li><a href="#">Pages</a>
                                        <ul>
                                            <li><a href="team.html">Team</a></li>
                                            <li><a href="pricing.html">Pricing</a></li>
                                            <li><a href="faqs.html">Faqs</a></li>
                                            <li><a href="process.html">Process</a></li>
                                            <li><a href="product-external-affiliate.html">Product external affiliate</a></li>
                                            <li><a href="shop-catgrid-filter-full.html">Shop catgrid filter full</a></li>
                                            <li><a href="shop-filter-full.html">Shop filter full</a></li>
                                            <li><a href="shop-left-sidebar.html">Shop left sidebar</a></li>
                                            <li><a href="404-page.html">404 page</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Features</a>
                                        <ul>
                                            <li><a href="gallery-style.html">Gallery Style</a></li>
                                            <li><a href="blog-style.html">Blog Style</a></li>
                                            <li><a href="testimonials.html">Testimonials</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Blog</a>
                                        <ul>
                                            <li><a href="blog-post1.html">Blog Post 1</a></li>
                                            <li><a href="blog-post2.html">Blog Post 2</a></li>
                                            <li><a href="blog-details.html">Blog Details</a></li>
                                        </ul>
                                    </li> -->
                                    <li><a href="contact.html">Contacto</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-lg-1 col-md-1">
                            <div class="header-search">
                                <form>
                                    <input type="text" class="search-input search-form" placeholder="Search...." required="">
                                    <a href="#" id="search-button"  class="search-button"><i class="icofont icofont-search"></i></a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>